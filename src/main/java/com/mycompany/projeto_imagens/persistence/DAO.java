package com.mycompany.projeto_imagens.persistence;

import com.mycompany.projeto_imagens.factory.DaoFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class DAO {
    public DAO() {
        Connection connection = null;
        try {
            connection = DaoFactory.connect();
            createTable_Usuarios();
            createTable_Permissoes();
            createTable_Imagens();
        } catch(RuntimeException error) {
            JOptionPane.showMessageDialog(null, "Não foi criar as tabelas no banco: " + error, "Erro de criação!", JOptionPane.ERROR_MESSAGE);
        } finally {
            DaoFactory.disconnect(connection);
        }
    }
    
    // Tabela imagens
    private void createTable_Imagens() {
        String query = "CREATE TABLE IF NOT EXISTS imagens("
                + "id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "diretorio VARCHAR NOT NULL, "
                + "historico VARCHAR NOT NULL, "
                + "idCriador INTEGER NOT NULL"
                + ")";
        
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        
        try {
            connection = DaoFactory.connect();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.execute();
        } catch (SQLException error) {
            JOptionPane.showMessageDialog(null, "Não foi possível criar a tabela de imagens no banco: " + error, "Erro de criação!", JOptionPane.ERROR_MESSAGE);
        } finally {
            DaoFactory.disconnect(connection, preparedStatement);
        }
    }
    
    public void insereImagem(String diretorio, int idCriador) {
        String sql = "INSERT INTO imagens("
                + "diretorio, " 
                + "historico, "               
                + "idCriador"
                + ") VALUES(?,?,?)";

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DaoFactory.connect();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, diretorio);
            preparedStatement.setString(2, "");            
            preparedStatement.setInt(3, idCriador);            
            preparedStatement.executeUpdate();
        } catch (SQLException error) {
            JOptionPane.showMessageDialog(null, "Não foi inserir a imagem no banco: " + error, "Erro de permissão!", JOptionPane.ERROR_MESSAGE);
        } finally {
            DaoFactory.disconnect(connection, preparedStatement);
        }
    }
    
    public JSONObject getImagemById(int imagemId) throws JSONException {
        JSONObject imageJSON = new JSONObject();
        
        String sql = "SELECT * FROM imagens "
                + "WHERE id = ? ";
        
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        
        try {
            connection = DaoFactory.connect();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, imagemId);
            result = preparedStatement.executeQuery();
            while (result.next()) {
                imageJSON.put("id", result.getInt("id"));
                imageJSON.put("diretorio", result.getString("diretorio"));
                imageJSON.put("historico", result.getString("historico"));
                imageJSON.put("criador", getNameOfUser(result.getInt("idCriador")));
            }
        } catch (SQLException error) {
            JOptionPane.showMessageDialog(null, "Não foi possível pesquisar as imagens que o usuário criou: " + error, "Erro de busca!", JOptionPane.ERROR_MESSAGE);
        } finally {
            DaoFactory.disconnect(connection, preparedStatement);
        }
        
        return imageJSON;
    }
    
    public JSONObject getImagemByIdAndName(String textoBusca, int imagemId) throws JSONException {
        JSONObject imageJSON = new JSONObject();
        
        String sql = "SELECT * FROM imagens "
                + "WHERE id = ? and nome LIKE ?";
        
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        
        try {
            connection = DaoFactory.connect();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, imagemId);
            preparedStatement.setString(2, textoBusca);
            result = preparedStatement.executeQuery();
            while (result.next()) {
                imageJSON.put("id", result.getInt("id"));
                imageJSON.put("diretorio", result.getString("diretorio"));
                imageJSON.put("historico", result.getString("historico"));
                imageJSON.put("criador", getNameOfUser(result.getInt("idCriador")));
            }
        } catch (SQLException error) {
            JOptionPane.showMessageDialog(null, "Não foi possível pesquisar as imagens que o usuário criou: " + error, "Erro de busca!", JOptionPane.ERROR_MESSAGE);
        } finally {
            DaoFactory.disconnect(connection, preparedStatement);
        }
        
        return imageJSON;
    }
    
    public ArrayList<String> getImagensList(int userId) {
        ArrayList<String> imagensList = new ArrayList<>();

        // pegando todas as imagens que o usuário criou
        String sql = "SELECT * FROM imagens "
                + "WHERE idCriador = ? "
                + "ORDER BY id DESC";

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
            
        try {
            connection = DaoFactory.connect();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, userId);
            result = preparedStatement.executeQuery();
            while (result.next()) {
                imagensList.add(result.getString("diretorio"));
            }
        } catch (SQLException error) {
            JOptionPane.showMessageDialog(null, "Não foi possível buscar as imagens que o usuário criou: " + error, "Erro de busca!", JOptionPane.ERROR_MESSAGE);
        } finally {
            DaoFactory.disconnect(connection, preparedStatement);
        }
        
        // pegando todas as imagens que o usuário tem permissão
        sql = "SELECT * FROM permissoes "
                + "WHERE idUsuario = ? "
                + "ORDER BY id DESC";
        
        connection = null;
        preparedStatement = null;
        result = null;
            
        try {
            connection = DaoFactory.connect();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, userId);
            result = preparedStatement.executeQuery();
            while (result.next()) {
                imagensList.add(result.getString("diretorio"));
            }
        } catch (SQLException error) {
            JOptionPane.showMessageDialog(null, "Não foi possível buscar as imagens que o usuário tem permissão: " + error, "Erro de busca!", JOptionPane.ERROR_MESSAGE);
        } finally {
            DaoFactory.disconnect(connection, preparedStatement);
        }
        
        return imagensList;
    }
    
    public JSONArray getImagensByName(int userId, String textoBusca) throws JSONException {
        JSONArray jArray = new JSONArray();

        // pegando todas as imagens que o usuário criou
        String sql = "SELECT * FROM imagens "
                + "WHERE idCriador = ? "
                + "WHERE diretorio LIKE ?"
                + "ORDER BY id DESC";

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
            
        try {
            connection = DaoFactory.connect();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, userId);
            preparedStatement.setString(2, textoBusca);
            result = preparedStatement.executeQuery();
            while (result.next()) {
                JSONObject imageJSON = new JSONObject();
                imageJSON.put("id", result.getInt("id"));
                imageJSON.put("diretorio", result.getString("diretorio"));
                imageJSON.put("historico", result.getString("historico"));
                imageJSON.put("criador", getNameOfUser(result.getInt("idCriador")));
                jArray.put(imageJSON);
            }
        } catch (SQLException error) {
            JOptionPane.showMessageDialog(null, "Não foi possível buscar as imagens que o usuário criou: " + error, "Erro de busca!", JOptionPane.ERROR_MESSAGE);
        } finally {
            DaoFactory.disconnect(connection, preparedStatement);
        }
        
        // pegando todas as imagens que o usuário tem permissao
        sql = "SELECT * FROM permissoes "
                + "WHERE idUsuario = ? "
                + "ORDER BY idImagem DESC";

        connection = null;
        preparedStatement = null;
        result = null;
            
        try {
            connection = DaoFactory.connect();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, userId);
            result = preparedStatement.executeQuery();
            while (result.next()) {
                JSONObject imagemObj = getImagemByIdAndName(textoBusca, result.getInt("idImagem"));
                jArray.put(imagemObj);
            }
        } catch (SQLException error) {
            JOptionPane.showMessageDialog(null, "Não foi possível buscar as imagens que o usuário tem permissão: " + error, "Erro de busca!", JOptionPane.ERROR_MESSAGE);
        } finally {
            DaoFactory.disconnect(connection, preparedStatement);
        }

        return jArray;
    }
    
    public JSONArray getImagens(int userId) throws JSONException {
        JSONArray jArray = new JSONArray();

        // pegando todas as imagens que o usuário criou
        String sql = "SELECT * FROM imagens "
                + "WHERE idCriador = ? "
                + "ORDER BY id DESC";

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
            
        try {
            connection = DaoFactory.connect();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, userId);
            result = preparedStatement.executeQuery();
            while (result.next()) {
                JSONObject imageJSON = new JSONObject();
                imageJSON.put("id", result.getInt("id"));
                imageJSON.put("diretorio", result.getString("diretorio"));
                imageJSON.put("historico", result.getString("historico"));
                imageJSON.put("criador", getNameOfUser(result.getInt("idCriador")));
                jArray.put(imageJSON);
            }
        } catch (SQLException error) {
            JOptionPane.showMessageDialog(null, "Não foi possível buscar as imagens que o usuário criou: " + error, "Erro de busca!", JOptionPane.ERROR_MESSAGE);
        } finally {
            DaoFactory.disconnect(connection, preparedStatement);
        }
        
        // pegando todas as imagens que o usuário tem permissao
        sql = "SELECT * FROM permissoes "
                + "WHERE idUsuario = ? "
                + "ORDER BY idImagem DESC";

        connection = null;
        preparedStatement = null;
        result = null;
            
        try {
            connection = DaoFactory.connect();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, userId);
            result = preparedStatement.executeQuery();
            while (result.next()) {
                JSONObject imagemObj = getImagemById(result.getInt("idImagem"));
                jArray.put(imagemObj);
            }
        } catch (SQLException error) {
            JOptionPane.showMessageDialog(null, "Não foi possível buscar as imagens que o usuário tem permissão: " + error, "Erro de busca!", JOptionPane.ERROR_MESSAGE);
        } finally {
            DaoFactory.disconnect(connection, preparedStatement);
        }

        return jArray;
    }
    
    // Tabela usuarios
    private void createTable_Usuarios() {
        String query = "CREATE TABLE IF NOT EXISTS usuarios("
                + "id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "nome VARCHAR NOT NULL, "
                + "senha VARCHAR NOT NULL, "
                + "notificacoes VARCHAR NOT NULL, "
                + "isAdmin BOOLEAN NOT NULL"
                + ")";
        
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        
        try {
            connection = DaoFactory.connect();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.execute();
        } catch (SQLException error) {
            JOptionPane.showMessageDialog(null, "Não foi criar a tabela de usuários no banco: " + error, "Erro de criação!", JOptionPane.ERROR_MESSAGE);
        } finally {
            DaoFactory.disconnect(connection, preparedStatement);
        }
        
        if (isUser("admin", "admin") == -1) {
            insereUsuario("admin", "admin", true);
        }
    }
    
    public JSONArray getUsuariosByName(String textoBusca) throws JSONException {
        JSONArray jArray = new JSONArray();

        String sql = "SELECT * FROM usuarios "
                + "WHERE nome LIKE ?"
                + "ORDER BY id DESC";

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        
        try {
            connection = DaoFactory.connect();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, "%" + textoBusca + "%");
            result = preparedStatement.executeQuery();
            
            while (result.next()) {
                JSONObject userJSON = new JSONObject();
                userJSON.put("id", result.getInt("id"));
                userJSON.put("nome", result.getString("nome"));
                
                if (result.getBoolean("isAdmin") == true) {
                    userJSON.put("administrador", "Sim");
                } else {
                    userJSON.put("administrador", "Não");
                }
                
                jArray.put(userJSON);
            }
            
        } catch (SQLException error) {
            JOptionPane.showMessageDialog(null, "Não foi possível buscar os usuários no banco: " + error, "Erro de busca!", JOptionPane.ERROR_MESSAGE);
        } finally {
            DaoFactory.disconnect(connection, preparedStatement);
        }

        return jArray;
    }
    
    public JSONArray getUsuarios() throws JSONException {
        JSONArray jArray = new JSONArray();

        String sql = "SELECT * FROM usuarios "
               + "ORDER BY id DESC";

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
        
        try {
            connection = DaoFactory.connect();
            preparedStatement = connection.prepareStatement(sql);
            result = preparedStatement.executeQuery();
            
            while (result.next()) {
                JSONObject userJSON = new JSONObject();
                userJSON.put("id", result.getInt("id"));
                userJSON.put("nome", result.getString("nome"));
                
                if (result.getBoolean("isAdmin") == true) {
                    userJSON.put("administrador", "Sim");
                } else {
                    userJSON.put("administrador", "Não");
                }
                
                jArray.put(userJSON);
            }
            
        } catch (SQLException error) {
            JOptionPane.showMessageDialog(null, "Não foi possível buscar os usuários no banco: " + error, "Erro de busca!", JOptionPane.ERROR_MESSAGE);
        } finally {
            DaoFactory.disconnect(connection, preparedStatement);
        }

        return jArray;
    }
    
    public int isUser(String nome, String senha) {  // retorna -1 se não encontrar, e o id se encontrar
        int userId = -1;
        
        String sql = "SELECT * FROM usuarios "
                + "WHERE nome = ? and senha = ?";

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
            
        try {
            connection = DaoFactory.connect();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, nome);
            preparedStatement.setString(2, senha);
            result = preparedStatement.executeQuery();
            while (result.next()) {
                userId = result.getInt("id");
            }
        } catch (SQLException error) {
            JOptionPane.showMessageDialog(null, "Não foi possível buscar o usuário: " + error, "Erro de busca!", JOptionPane.ERROR_MESSAGE);
        } finally {
            DaoFactory.disconnect(connection, preparedStatement);
        }
        
        if (!nome.equals("admin") && !senha.equals("admin") && userId == -1) {
            JOptionPane.showMessageDialog(null, "Usuário não encontrado", "Erro de login!", JOptionPane.ERROR_MESSAGE);
        }
        
        return userId;
    }
    
    public void insereUsuario(String nome, String senha, boolean isAdmin) {
        String sql = "INSERT INTO usuarios("
                + "nome, " 
                + "senha, "               
                + "notificacoes, "
                + "isAdmin"
                + ") VALUES(?,?,?,?)";
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DaoFactory.connect();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, nome);
            preparedStatement.setString(2, senha);            
            preparedStatement.setString(3, "");            
            preparedStatement.setBoolean(4, isAdmin);
            preparedStatement.executeUpdate();
        } catch (SQLException error) {
            JOptionPane.showMessageDialog(null, "Não foi inserir a permissão no banco: " + error, "Erro de permissão!", JOptionPane.ERROR_MESSAGE);
        } finally {
            DaoFactory.disconnect(connection, preparedStatement);
        }
    }
    
    public void excluiUsuario(int id) {
        String sql = "DELETE FROM usuarios WHERE id = ?";
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DaoFactory.connect();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException error) {
            JOptionPane.showMessageDialog(null, "Não foi excluir o usuário: " + error, "Erro de exclusão!", JOptionPane.ERROR_MESSAGE);
        } finally {
            DaoFactory.disconnect(connection, preparedStatement);
        }
    }
    
    public void setIsUserAdmin(int id, boolean isAdmin) {
        String sql = "UPDATE usuarios SET "
                + "isAdmin = ?, "
                + "WHERE id = ?";
        
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try { 
            connection = DaoFactory.connect();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            preparedStatement.setBoolean(2, isAdmin);
            preparedStatement.executeUpdate();
        } catch (SQLException error) {
            JOptionPane.showMessageDialog(null, "Não foi atualizar o status de admin do usuário: " + error, "Erro de atualização!", JOptionPane.ERROR_MESSAGE);
        } finally {
            DaoFactory.disconnect(connection, preparedStatement);
        }
    }
    
    public boolean isUserAdmin(int userId) {
        boolean isAdmin = false;
        
        String sql = "SELECT * FROM usuarios "
                + "WHERE id = ? ";

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
            
        try {
            connection = DaoFactory.connect();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, userId);
            result = preparedStatement.executeQuery();
            while (result.next()) {
                isAdmin = result.getBoolean("isAdmin");
            }
        } catch (SQLException error) {
            JOptionPane.showMessageDialog(null, "Não foi possível verificar se o usuário é administrador: " + error, "Erro de verificação!", JOptionPane.ERROR_MESSAGE);
        } finally {
            DaoFactory.disconnect(connection, preparedStatement);
        }
        
        return isAdmin;
    }
    
    public String getNotificacoes(int userId) {
        String notificacoes = "";
        
        String sql = "SELECT * FROM usuarios "
                + "WHERE id = ? ";

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
            
        try {
            connection = DaoFactory.connect();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, userId);
            result = preparedStatement.executeQuery();
            while (result.next()) {
                notificacoes = result.getString("notificacoes");
            }
        } catch (SQLException error) {
            JOptionPane.showMessageDialog(null, "Não foi possível buscar as notificações: " + error, "Erro de busca!", JOptionPane.ERROR_MESSAGE);
        } finally {
            DaoFactory.disconnect(connection, preparedStatement);
        }
        
        return notificacoes;
    }
    
    public String getNameOfUser(int userId) {
        String name = "";
        
        String sql = "SELECT * FROM usuarios "
                + "WHERE id = ? ";

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet result = null;
            
        try {
            connection = DaoFactory.connect();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, userId);
            result = preparedStatement.executeQuery();
            while (result.next()) {
                name = result.getString("nome");
            }
        } catch (SQLException error) {
            JOptionPane.showMessageDialog(null, "Não foi possível verificar se o usuário é administrador: " + error, "Erro de verificação!", JOptionPane.ERROR_MESSAGE);
        } finally {
            DaoFactory.disconnect(connection, preparedStatement);
        }
        
        return name;
    }
    
    // Tabela permissoes
    private void createTable_Permissoes() {
        String query = "CREATE TABLE IF NOT EXISTS permissoes("
                + "idImagem INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "idUsuario INTEGER NOT NULL"
                + ")";
        
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        
        try {
            connection = DaoFactory.connect();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.execute();
            
        } catch (SQLException error) {
            JOptionPane.showMessageDialog(null, "Não foi criar a tabela de permissões no banco: " + error, "Erro de criação!", JOptionPane.ERROR_MESSAGE);
        } finally {
            DaoFactory.disconnect(connection, preparedStatement);
        }
    }
    
    public void inserePermissao(int idImagem, int idUsuario) {
        String sql = "INSERT INTO permissoes("
                + "idImagem, " 
                + "idUsuario"
                + ") VALUES(?,?)";
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DaoFactory.connect();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, idImagem);
            preparedStatement.setInt(2, idUsuario);
            preparedStatement.executeUpdate();
        } catch (SQLException error) {
            JOptionPane.showMessageDialog(null, "Não foi inserir a permissão no banco: " + error, "Erro de permissão!", JOptionPane.ERROR_MESSAGE);
        } finally {
            DaoFactory.disconnect(connection, preparedStatement);
        }
    }
}
