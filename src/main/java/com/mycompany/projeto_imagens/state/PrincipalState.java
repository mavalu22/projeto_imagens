package com.mycompany.projeto_imagens.state;

import com.mycompany.projeto_imagens.persistence.DAO;
import com.mycompany.projeto_imagens.view.TelaBuscar;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class PrincipalState {
    private TelaBuscar view;
    private JTable tabela;
    private int userId;
    
    public PrincipalState(int userId, String userName, boolean isAdmin) {
        this.view = new TelaBuscar();
        this.view.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        
        this.tabela = this.view.getTabela();
        this.userId = userId;
        this.view.setUserFooter(userName, isAdmin);
        
        this.view.addWindowListener(new WindowAdapter() { 
            @Override
            public void windowClosing(WindowEvent e) { 
                new LoginState();
            }
        });
        
        this.view.getBotaoBuscar().addActionListener((e) -> buscar());
        
        this.view.getBotaoNovo().setVisible(view.getTipoBusca() == 1);
        this.view.getBotaoNovo().addActionListener((e) -> {
            new EditarState();
        });
        
        this.view.getBotaoFechar().addActionListener((e) -> {
            new LoginState();
            this.view.dispose();
        });
        
        this.view.getBotaoVisualizar().setVisible(view.getTipoBusca() == 1);
        this.view.getBotaoVisualizar().addActionListener((e) -> {
            new VisualizarState();
        });
        
        this.view.getComboBoxTipoBusca().addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                if (evt.getStateChange() == java.awt.event.ItemEvent.SELECTED) {
                    view.getBotaoManterUsuarios().setVisible(isAdmin && view.getTipoBusca() == 0);
                    view.getBotaoVisualizarImagensDaPasta().setVisible(view.getTipoBusca() == 1);
                    view.getBotaoVisualizar().setVisible(view.getTipoBusca() == 1);
                    view.getBotaoNovo().setVisible(view.getTipoBusca() == 1);
                    checkNotifications(userId);
                    buscar();
                }
            } 
        });
        
        this.view.getBotaoManterUsuarios().setVisible(isAdmin && this.view.getTipoBusca() == 0);
        this.view.getBotaoManterUsuarios().addActionListener((e) -> {
            new ManterUsuariosState();
        });
        
        this.view.getBotaoNotificacoes().addActionListener((e) -> {
            JOptionPane.showMessageDialog(null, new DAO().getNotificacoes(userId), "Notificações", JOptionPane.INFORMATION_MESSAGE);
        });
        
        this.view.getBotaoVisualizarImagensDaPasta().setVisible(view.getTipoBusca() == 1);
        this.view.getBotaoVisualizarImagensDaPasta().addActionListener((e) -> {
            JFileChooser chooser = new JFileChooser();
            chooser.setCurrentDirectory(new java.io.File("."));
            chooser.setDialogTitle("Escolha uma pasta");
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            chooser.setAcceptAllFileFilterUsed(false);

            if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
              visualizarImagensDaPasta(chooser.getCurrentDirectory().toString());
            }
        });
                
        checkNotifications(userId);
        buscar();
        this.view.setVisible(true);
    }
    
    private void checkNotifications(int userId) {
        if (new DAO().getNotificacoes(userId).equals("")) {
            this.view.getBotaoNotificacoes().setVisible(false);
        } else {
            this.view.getBotaoNotificacoes().setVisible(true);
        }
    }
    
    public void visualizarImagensDaPasta(String Diretorio) {
        try (Stream<Path> paths = Files.walk(Paths.get(Diretorio))) {
            List<Path> newPaths = paths.collect(Collectors.toList());
            for(Path newPath: newPaths){
                if(newPath.toString().contains(".jpeg")){
                    // listar todas as imagens em miniatura
                    // pegar quais delas já foram adicionadas e o usuário não tem permissão
                    // pegar quais delas ele vai pedir permissão, e quais não 
                    // se (a imagem ainda não foi adicionada no sistema) {
                        DAO dao = new DAO();
                        dao.insereImagem(newPath.toString(), this.userId);
//                        dao.insereHistorico(
//                            dao.getImagemIdByName(newPath.toString()),
//                            "Imagem adicionada por " + this.userId + " em " + LocalDate.now().toString()
//                        );
                        buscar();
                    // }
                }
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Não foi possível ler o caminho da pasta: " + ex, "Erro de leitura!", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    // pesquisa
    private void buscar() {
        String textoBusca = this.view.getCampoBuscar().getText();

        if (this.view.getTipoBusca() == 0) {
            if(textoBusca.isEmpty() || textoBusca.equals("")) {
                atualizaTabelaUsuarios();
            } else {
                pesquisaUsuarios(textoBusca);
            }
        } else {
            if (this.view.getTipoBusca() == 1) {
                if(textoBusca.isEmpty() || textoBusca.equals("")) {
                    atualizaTabelaImagens();
                } else {
                    pesquisaImagens(textoBusca);
                }
            }  
        }
    }
    
    private void pesquisaUsuarios(String textoBusca) {
        setTableHeader("Usuários");
        
        try { 
            DefaultTableModel tabelaModel = (DefaultTableModel) this.tabela.getModel();
            
            tabelaModel.setNumRows(0);

            JSONArray jUsuarios = new DAO().getUsuariosByName(textoBusca);

            for (int i = 0; i < jUsuarios.length(); ++i) {
                JSONObject rec = jUsuarios.getJSONObject(i);
                tabelaModel.addRow(new Object[]{
                    rec.getInt("id"),
                    rec.getString("nome"),
                    rec.getString("administrador"),
                });
            }
            
        } catch(JSONException e) {
            JOptionPane.showMessageDialog(null, "Não foi possível atualizar a tabela de usuários: " + e, "Erro de atualização!", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    private void pesquisaImagens(String textoBusca) {
        setTableHeader("Imagens");
        
        try { 
            DefaultTableModel tabelaModel = (DefaultTableModel) this.tabela.getModel();
            
            tabelaModel.setNumRows(0);

            JSONArray jImagens = new DAO().getImagensByName(this.userId, textoBusca);

            for (int i = 0; i < jImagens.length(); ++i) {
                JSONObject rec = jImagens.getJSONObject(i);
                tabelaModel.addRow(new Object[]{
                    rec.getInt("id"),
                    rec.getString("diretorio"),
                    rec.getString("historico"),
                    rec.getString("criador"),
                });
            }
            
        } catch(JSONException e) {
            JOptionPane.showMessageDialog(null, "Não foi possível atualizar a tabela de imagens: " + e, "Erro de atualização!", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    // tabela
    private void atualizaTabelaUsuarios() {
        setTableHeader("Usuários");
        
        try { 
            DefaultTableModel tabelaModel = (DefaultTableModel) this.tabela.getModel();
            
            tabelaModel.setNumRows(0);

            JSONArray jUsuarios = new DAO().getUsuarios();

            for (int i = 0; i < jUsuarios.length(); ++i) {
                JSONObject rec = jUsuarios.getJSONObject(i);
                tabelaModel.addRow(new Object[]{
                    rec.getInt("id"),
                    rec.getString("nome"),
                    rec.getString("administrador"),
                });
            }
            
        } catch(JSONException e) {
            JOptionPane.showMessageDialog(null, "Não foi possível atualizar a tabela de usuários: " + e, "Erro de atualização!", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    private void setTableHeader(String type) {
        if(type.equals("Usuários")) {
            this.tabela.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {
                    {null, null, null},
                    {null, null, null},
                    {null, null, null},
                    {null, null, null}
                },
                new String [] {
                    "id", "Nome", "Administrador"
                }
            ));
        } else {
            this.tabela.setModel(new javax.swing.table.DefaultTableModel(
                new Object [][] {
                    {null, null, null, null},
                    {null, null, null, null},
                    {null, null, null, null},
                    {null, null, null, null}
                },
                new String [] {
                    "id", "Diretorio", "Histórico", "Criador"
                }
            ));
        }
    }
    
    private void atualizaTabelaImagens() {
        setTableHeader("Imagens");
        
        try { 
            DefaultTableModel tabelaModel = (DefaultTableModel) this.tabela.getModel();
            
            tabelaModel.setNumRows(0);

            JSONArray jImagens = new DAO().getImagens(this.userId);

            for (int i = 0; i < jImagens.length(); ++i) {
                JSONObject rec = jImagens.getJSONObject(i);
                tabelaModel.addRow(new Object[]{
                    rec.getInt("id"),
                    rec.getString("diretorio"),
                    rec.getString("historico"),
                    rec.getString("criador"),
                });
            }
            
        } catch(JSONException e) {
            JOptionPane.showMessageDialog(null, "Não foi possível atualizar a tabela de imagens: " + e, "Erro de atualização!", JOptionPane.ERROR_MESSAGE);
        }
    }
}
