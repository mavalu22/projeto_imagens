package com.mycompany.projeto_imagens.state;

import com.mycompany.projeto_imagens.view.TelaConfirmarExclusao;
import com.mycompany.projeto_imagens.view.TelaExclusaoRealizada;
import com.mycompany.projeto_imagens.view.TelaManter_Visualizacao;

public class VisualizarState {
    private TelaManter_Visualizacao view;
    
    private TelaConfirmarExclusao viewConfirmarExclusao;
    private TelaExclusaoRealizada viewExclusaoRealizada;
    
    public VisualizarState() {
        this.view = new TelaManter_Visualizacao();
        this.view.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        this.viewConfirmarExclusao = new TelaConfirmarExclusao();
        this.viewExclusaoRealizada = new TelaExclusaoRealizada();
        
        this.view.getBotaoEditar().addActionListener((e) -> {
            this.view.setVisible(false);
            new EditarState();
        });
        
        this.view.getBotaoExcluir().addActionListener((e) -> {
             this.viewConfirmarExclusao.setVisible(true);
        });
        
        this.view.getBotaoFechar().addActionListener((e) -> {
            this.view.setVisible(false);
        });
        
        // confirmar exclusao
        this.viewConfirmarExclusao.getBotaoConfirmaExclusao().addActionListener((e) -> {
            this.viewConfirmarExclusao.setVisible(false);
            this.viewExclusaoRealizada.setVisible(true);
        });
        
        this.viewConfirmarExclusao.getBotaoNegaExclusao().addActionListener((e) -> {
            this.viewConfirmarExclusao.setVisible(false);
        });
        
        // exclusão realizada
        this.viewExclusaoRealizada.getBotaoExclusaoRealizada().addActionListener((e) -> {
            this.viewExclusaoRealizada.setVisible(false);
        });
        
        this.view.setVisible(true);
    }
}
