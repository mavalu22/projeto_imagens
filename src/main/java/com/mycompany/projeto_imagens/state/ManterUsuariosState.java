package com.mycompany.projeto_imagens.state;

import com.mycompany.projeto_imagens.persistence.DAO;
import com.mycompany.projeto_imagens.view.TelaManterUsuario;

public class ManterUsuariosState {
    private TelaManterUsuario view;
    
    public ManterUsuariosState() {
        this.view = new TelaManterUsuario();        
        this.view.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        
        this.view.getBotaoCriarUsuario().addActionListener((e) -> {
            new DAO().insereUsuario(
                this.view.getTextoNome().getText(),
                new String(this.view.getTextoSenha().getPassword()),
                this.view.getIsAdminCheckBox().isSelected()
            );
        });
        
        this.view.setVisible(true);
    }
}