package com.mycompany.projeto_imagens.state;

import com.mycompany.projeto_imagens.view.TelaManter_Edicao;
import com.mycompany.projeto_imagens.view.TelaSucesso;

public class EditarState {
    private TelaManter_Edicao view;
    private TelaSucesso viewSucesso;

    
    public EditarState() {
        this.view = new TelaManter_Edicao(); 
        this.view.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        this.viewSucesso = new TelaSucesso();
        
        this.view.getBotaoCancelar().addActionListener((e) -> {
            this.view.setVisible(false);
            new VisualizarState();
        });
        
        this.view.getBotaoSalvar().addActionListener((e) -> {
            this.viewSucesso.setVisible(true);
        });
        
        // sucesso
        this.viewSucesso.getBotaoSucesso().addActionListener((e) -> {
            this.viewSucesso.setVisible(false);
        });
        
        this.view.setVisible(true);
    }
}