package com.mycompany.projeto_imagens.state;

import com.mycompany.projeto_imagens.persistence.DAO;
import com.mycompany.projeto_imagens.view.TelaLogin;

public class LoginState {
    private TelaLogin view;
    
    public LoginState() {
        this.view = new TelaLogin();        
        this.view.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        
        this.view.getBotaoEntrar().addActionListener((e) -> {
            DAO dao = new DAO();
            
            int userId = dao.isUser(this.view.getNome(), this.view.getSenha());
            
            if (userId != -1) {
                new PrincipalState(userId, this.view.getNome(), dao.isUserAdmin(userId));
                this.view.setVisible(false);
            }
        });
        
        this.view.setVisible(true);
    }
}