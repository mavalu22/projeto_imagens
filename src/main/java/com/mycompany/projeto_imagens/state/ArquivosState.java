package com.mycompany.projeto_imagens.state;

import com.mycompany.projeto_imagens.view.TelaArquivos;
import javax.swing.JFileChooser;

public class ArquivosState {
    private TelaArquivos view;
    
    public ArquivosState() {
        this.view = new TelaArquivos();
        this.view.getjFileChooser1().setVisible(true);
        this.view.setVisible(true);
    }
    
    public JFileChooser getFileChooser() {
        return this.view.getjFileChooser1();
    }
}
