package com.mycompany.projeto_imagens.factory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class DaoFactory {
    private static final String URL = "jdbc:sqlite:banco.db";
    
    public static Connection connect() {
        try {
            return DriverManager.getConnection(URL);
        }         
        catch(SQLException error) {
            JOptionPane.showMessageDialog(null, "Não foi possível se conectar ao banco: " + error, "Erro de conexão!", JOptionPane.ERROR_MESSAGE);
            return null;
        }
    }

    
    public static void disconnect(Connection connection) {
        try {
            if(connection != null) {
                connection.close();
            }
        } catch (SQLException error) {
            JOptionPane.showMessageDialog(null, "Não foi possível se desconectar do banco: " + error, "Erro de conexão!", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public static void disconnect(Connection connection, PreparedStatement preparedStatement) {
        disconnect(connection);
        try {
            if(preparedStatement != null) {
                preparedStatement.close();
            }
        } catch (SQLException error) {
            JOptionPane.showMessageDialog(null, "Não foi possível se desconectar do banco: " + error, "Erro de conexão!", JOptionPane.ERROR_MESSAGE);
        }
    }
}
