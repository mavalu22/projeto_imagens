import com.mycompany.projeto_imagens.proxy.IImagem;
import com.mycompany.projeto_imagens.proxy.Imagem;

public class ImagemProxy implements IImagem {
    public static IImagem imagem;

    @Override
    public void getImagem(String Diretorio) {
        if (imagem == null) {
            this.imagem = new Imagem(Diretorio);
        }
        imagem.getImagem(Diretorio);
    }
}