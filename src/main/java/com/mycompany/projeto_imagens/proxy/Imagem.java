package com.mycompany.projeto_imagens.proxy;

import com.mycompany.projeto_imagens.model.ImagemModel;
import com.mycompany.projeto_imagens.persistence.DAO;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;
import javax.swing.JOptionPane;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class Imagem implements IImagem {

    public Imagem(String Diretorio) {
        getImagem(Diretorio);
    }
    
    public Imagem(int idImagem) {
        getImagem(leituraDisco(idImagem));
    }
    
    @Override
    public void getImagem(String Diretorio) {       
        try {
            new ImagemModel(Diretorio);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Não foi possível gerar a imagem: " + ex, "IOException", JOptionPane.ERROR_MESSAGE);
        } catch (InterruptedException ex) {
            JOptionPane.showMessageDialog(null, "Não foi possível gerar a imagem: " + ex, "InterruptedException", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public String leituraDisco(int idImagem) {
        try {
            JSONObject imagemDisco =  new DAO().getImagemById(idImagem);
            return imagemDisco.getString("diretorio");
        } catch (JSONException ex) {
            JOptionPane.showMessageDialog(null, "Não foi possível ler a imagem do disco: " + ex, "JSONException", JOptionPane.ERROR_MESSAGE);
            return "";
        }
    }
}
